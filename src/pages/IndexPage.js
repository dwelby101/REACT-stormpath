
import { Link } from 'react-router';
import React, { PropTypes } from 'react';
import DocumentTitle from 'react-document-title';

import { LoginLink } from 'react-stormpath';
import { LogoutLink } from 'react-stormpath';


export default class IndexPage extends React.Component {
    render() {
        return (
            <div className="container">
                <h2 className="text-center">WelcomeTo Project Sapphire!</h2>
                <hr />
                <div className="jumbotron">
                    <p>
                        <strong>To my REACT-stormpath application!</strong>
                    </p>
                    <p>Ready to begin... Try these Stormpath features that are included in this example:</p>
                    <ol className="lead">
                        <li><Link to="/register">Registration</Link></li>
                        <li><LoginLink /></li>
                        <li><LogoutLink /></li>
                        <li><Link to="/profile">Custom Profile Data</Link></li>
                    </ol>
                </div>
            </div>
        );
    }

}