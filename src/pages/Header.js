
import React from 'react';
import { Link } from 'react-router';
import { LoginLink, LogoutLink, Authenticated, NotAuthenticated } from 'react-stormpath';
import Classnames from 'classnames';

export default class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hideMenu: true
            };
    }

    click() {
        this.setState({hideMenu: !this.state.hideMenu});
    }


    render() {

        var classes = Classnames({
                        'navbar-collapse' : true,
                        'collapse' : this.state.hideMenu
                    });

        return(
            <nav className="navbar navbar-default navbar-static-top">
                <div className="container">

                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" onClick={this.click.bind(this)}>
                            <span className="sr-only">Toggle Navigation</span>
                            <span className="icon-bar"></span><span className="icon-bar"></span><span className="icon-bar"></span>
                        </button>
                    </div>

                    <div className={classes}>

                        <ul className="nav navbar-nav">
                            <li><Link to="/">Home</Link></li>
                        </ul>

                        <ul className="nav navbar-nav navbar-right">

                            <NotAuthenticated>
                                <li>
                                    <LoginLink />
                                </li>
                            </NotAuthenticated>

                            <NotAuthenticated>
                                <li>
                                    <Link to="/register">Create Account</Link>
                                </li>
                            </NotAuthenticated>

                            <Authenticated>
                                <li>
                                    <Link to="/profile">Profile</Link>
                                </li>
                            </Authenticated>

                            <Authenticated>
                                <li>
                                    <LogoutLink />
                                </li>
                            </Authenticated>

                        </ul>

                    </div>
                </div>
            </nav>
        );
    }
}
