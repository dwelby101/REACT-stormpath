/*
import React from 'react';
import ReactDOM from 'react-dom';
import {IndexRoute, Route } from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import ReactStormpath, { Router, HomeRoute, LoginRoute, LogoutRoute, RegisterRoute, AuthenticatedRoute } from 'react-stormpath';
import { MasterPage, IndexPage, LoginPage, RegistrationPage, ProfilePage} from './pages';
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { IndexRoute, Route } from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import ReactStormpath, { Router, HomeRoute, LoginRoute, LogoutRoute, AuthenticatedRoute } from 'react-stormpath';
import { MasterPage, IndexPage, LoginPage, RegistrationPage, ResetPasswordPage, VerifyEmailPage, ProfilePage, bootstrap} from './pages';



ReactStormpath.init();

ReactDOM.render(
/*
    <Router history={ createBrowserHistory() } >
        <HomeRoute path='/' component={ MasterPage } >
            <IndexRoute component={ IndexPage } />
            <LoginRoute path='/login' component={ LoginPage } />

            <RegisterRoute path='/register' component={ RegistrationPage } />
            <AuthenticatedRoute>
                <Route path='/profile' component={ ProfilePage } />
            </AuthenticatedRoute>
        </HomeRoute>
    </Router>,
    document.getElementById('app-container')
);
*/


<Router history={createBrowserHistory()}>
    <HomeRoute path='/' component={MasterPage}>
        <IndexRoute component={IndexPage} />
        <LoginRoute path='/login' component={LoginPage} />
        <LogoutRoute path='/logout' />
        //<Route path='/verify' component={VerifyEmailPage} />

        <Route path='/register' component={RegistrationPage} />
        //<Route path='/forgot' component={ResetPasswordPage} />
        <AuthenticatedRoute>
            <HomeRoute path='/profile' component={ProfilePage} />
        </AuthenticatedRoute>
    </HomeRoute>
</Router>,

    document.getElementById('app-container')
);